﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using System.IO;
using System.Web.UI;
using System.Data;
using System.Data.SqlClient;
using SanitEntity;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Net;

namespace Sanit.Clases
{
    public static class Comun
    {
        #region Case Sensitive
        // static string UppercaseFirst(string s)

        public static string UppercaseFirst(string cadena)
        {
            if (string.IsNullOrEmpty(cadena))
            {
                return string.Empty;
            }
            cadena = cadena.ToLower();

            char[] UFirst = cadena.ToCharArray();
            UFirst[0] = char.ToUpper(UFirst[0]);
            return new string(UFirst);
        }


        #endregion

        #region Excel
        public static void ExportToExcel(HttpResponseBase Response, object dataList, string filename)
        {
            var grid = new System.Web.UI.WebControls.GridView();
            grid.DataSource = dataList;
            grid.DataBind();
            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment; filename=" + filename + ".xls");
            Response.ContentType = "application/excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            grid.RenderControl(htw);
            Response.Write(sw.ToString());
            Response.End();
        }
        #endregion

        #region Reportes
        public class ReportesParametros
        {
            public object ReportSource { set; get; }
            public string ReportPath { set; get; }
            public string NombreArchivo { get; set; }
        }

        public class ReportParameters
        {
            public static ReportesParametros DatosReporte
            {
                get { return (ReportesParametros)HttpContext.Current.Session["ReportParams"]; }
                set { HttpContext.Current.Session["ReportParams"] = value; }
            }
        }
        #endregion

        #region Formato Numeracion

        public static string FormatoNumeracion(int numero)
        {
            string strNum = "";
            if (numero < 10) { strNum = "000000" + numero; }
            if (numero > 9 && numero < 100) { strNum = "00000" + numero; }
            if (numero > 99 && numero < 1000) { strNum = "0000" + numero; }
            if (numero > 999 && numero < 10000) { strNum = "000" + numero; }
            if (numero > 9999 && numero < 100000) { strNum = "00" + numero; }
            if (numero > 99999 && numero < 1000000) { strNum = "0" + numero; }
            if (numero > 999999 && numero < 10000000) { strNum = "" + numero; }
            return strNum;
        }
        #endregion
        
		#region Login
        /// <summary>
        /// resumen MD5 en 4 octetos
        /// </summary>
        /// <param name="numeros"></param>
        /// <returns></returns>
        public static string MD5(string numeros)
        {
            string num = StringToMD5(numeros + "HAXORTANGOPYX").Trim().ToLower();
            num = num.Substring(0, 8) + " " + num.Substring(8, 8) + " " + num.Substring(16, 8) + " " + num.Substring(24, 8);
            return num;
        }
        /// <summary>
        /// resumen md5
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string StringToMD5(string s)
        {
            //Declarations
            string MD5String = null;
            byte[] EncStringBytes = null;
            UTF8Encoding Encoder = new UTF8Encoding();
            MD5CryptoServiceProvider MD5Hasher = new MD5CryptoServiceProvider();
            //Converts the String to bytes
            EncStringBytes = Encoder.GetBytes(s);
            //Generates the MD5 Byte Array
            EncStringBytes = MD5Hasher.ComputeHash(EncStringBytes);
            //Create MD5 hash
            MD5String = BitConverter.ToString(EncStringBytes);
            MD5String = MD5String.Replace("-", "");
            return MD5String;
        }
        #endregion

        #region Correo
        public static bool EnviarCorreo(string correoPara, string mensaje)
        {
            try
            {
                string smtp1 = string.Empty;
                string smtpuser = string.Empty;
                string smtppass = string.Empty;
                string smtpport = string.Empty;
                /*smtp1 = "smtp-mail.outlook.com";
                smtp1 = "smtp.gmail.com";
                smtpuser = "mari.insaurralde@hotmail.com";
                smtpuser = "marinainsaurralde11@gmail.com";
                smtpport = "587";*/

                //agregado temporal
                smtp1 = "mail.dinac.gov.py";
                smtpuser = "infosistema@dinac.gov.py";
                smtppass = "Dinac@123";
                smtpport = "25";

                string fromAddress = smtpuser;
                string mailPassword = smtppass;
                SmtpClient client = new SmtpClient();
                client.Port = int.Parse(smtpport);
                client.Host = smtp1;
                client.EnableSsl = true;
                client.Timeout = 20000;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Credentials = new System.Net.NetworkCredential(fromAddress, mailPassword);
                var send_mail = new MailMessage();
                send_mail.IsBodyHtml = true;
                send_mail.From = new MailAddress(fromAddress);
                send_mail.To.Add(new MailAddress(correoPara));
                send_mail.Subject = "Formulario Sanitario de Ingreso a Paraguay";
                send_mail.Body = mensaje;
                ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(ValidateServerCertificate);
                client.Send(send_mail);

                return true;
            }
            catch (Exception ex)
            {
                Logger.Log.Error("ERROR EN APLICACION ENVIAR CORREO " + ex.Message);
                return false;
            }
        }
        public static bool ValidateServerCertificate(Object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }
        #endregion

        #region Datatable
        //public DataTable DevuelveDatos(string sinsql, string sConn)
        //  {
        //      string strConexion;
        //      // Get the connectionStrings.
        //      ConnectionStringSettingsCollection connectionStrings = ConfigurationManager.ConnectionStrings;

        //      strConexion = connectionStrings[sConn].ToString();

        //      SqlConnection objConexion = new SqlConnection(strConexion);

        //      //inciamos el objeto command
        //      SqlCommand objCommand = new SqlCommand(sinsql, objConexion);

        //      objCommand.CommandTimeout = 0;

        //      //creamos el objeto dataAdapter
        //      SqlDataAdapter objAdapter = new SqlDataAdapter();

        //      //configuramos la propiedad select
        //      objAdapter.SelectCommand = objCommand;


        //      //abrimos la conexion
        //      objConexion.Open();

        //      //le decimos que dataset almacenar
        //      DataTable objDt = new DataTable();

        //      //rellenamos el datatable del dataset mediante fill
        //      objAdapter.Fill(objDt);
        //      //objDt.DataSet = objDs
        //      //cerramos la conexion
        //      objConexion.Close();

        //      return objDt;

        //  }
        #endregion

        //public static List<> Codigo() { 


        //}
    }
}
