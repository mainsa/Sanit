﻿using System.Web;

namespace Sanit.Reportes
{
    public static class Comun
    {

        #region Reportes
        public class ReportesParametros
        {
            public object ReportSource { set; get; }
            public string ReportPath { set; get; }
            public string NombreArchivo { get; set; }
        }

        public class ReportParameters
        {
            public static ReportesParametros DatosReporte
            {
                get { return (ReportesParametros)HttpContext.Current.Session["ReportParams"]; }
                set { HttpContext.Current.Session["ReportParams"] = value; }
            }
        }
        #endregion
    }
}
