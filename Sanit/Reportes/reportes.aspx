﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.4000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <script runat="server">
        void Page_Load(object sender, EventArgs e){

            CrystalDecisions.CrystalReports.Engine.ReportDocument CReport = new CrystalDecisions.CrystalReports.Engine.ReportDocument();
                if (Sanit.Reportes.Comun.ReportParameters.DatosReporte != null) {
                    string archivo = Sanit.Reportes.Comun.ReportParameters.DatosReporte.NombreArchivo;
                    CReport.Load(Sanit.Reportes.Comun.ReportParameters.DatosReporte.ReportPath);
                    CReport.SetDataSource(Sanit.Reportes.Comun.ReportParameters.DatosReporte.ReportSource);
                    CrystalReportViewer1.ReportSource = CReport;
                    CrystalReportViewer1.RefreshReport();
                    CReport.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, archivo);
                }
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" />
    </form>
</body>
</html>