﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Sanit
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{codigo}",
                defaults: new { controller = "Home", action = "Index", codigo = UrlParameter.Optional }
            );
            //routes.MapRoute(
            //    name: "Edit",
            //    url: "{controller}/{action}/{codigo}",
            //    defaults: new { controller = "Home", action = "EditarDatosPasajero", codigo = UrlParameter.Optional }
            //);
        }
    }
}
