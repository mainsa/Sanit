﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SanitEntity;

namespace Sanit.Controllers
{
    public class FormularioPrincipalController : Controller
    {
        private FormSanitEntities db = new FormSanitEntities();

        // GET: FormularioPrincipal
        public ActionResult Index()
        {
            return View(db.FormularioPrincipal.ToList());
        }

        // GET: FormularioPrincipal/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FormularioPrincipal formularioPrincipal = db.FormularioPrincipal.Find(id);
            if (formularioPrincipal == null)
            {
                return HttpNotFound();
            }
            return View(formularioPrincipal);
        }

        // GET: FormularioPrincipal/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: FormularioPrincipal/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "idpasajero,idLineaAerea,lineaAerea,numeroVuelo,numeroAsiento,fechaLlegada,apellido,nombre,inicial,sexo,celular,telefonoTrabajo,telefonoDomicilio,correoElectronico,direccionPermanente,numeroCasaPermanente,ciudadPermanente,depEstProvPermanente,idPaisPermanente,codigoPostalPermanente,nombreHotelTemp,direccionHotelTemp,numPiso,ciudadTemp,idDepartamentoTemp,idPaisTemp,codigoPostalTemp,apellidoContUrg,nombreContUrg,ciudadContUrg,idPaisContUrg,emailContUrg,celularContUrg,telefonoContUrg,contacto14dias,sintomas,hospital14dias,animalesVivos14Dias")] FormularioPrincipal formularioPrincipal)
        {
            if (ModelState.IsValid)
            {
                db.FormularioPrincipal.Add(formularioPrincipal);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(formularioPrincipal);
        }

        // GET: FormularioPrincipal/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FormularioPrincipal formularioPrincipal = db.FormularioPrincipal.Find(id);
            if (formularioPrincipal == null)
            {
                return HttpNotFound();
            }
            return View(formularioPrincipal);
        }

        // POST: FormularioPrincipal/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "idpasajero,idLineaAerea,lineaAerea,numeroVuelo,numeroAsiento,fechaLlegada,apellido,nombre,inicial,sexo,celular,telefonoTrabajo,telefonoDomicilio,correoElectronico,direccionPermanente,numeroCasaPermanente,ciudadPermanente,depEstProvPermanente,idPaisPermanente,codigoPostalPermanente,nombreHotelTemp,direccionHotelTemp,numPiso,ciudadTemp,idDepartamentoTemp,idPaisTemp,codigoPostalTemp,apellidoContUrg,nombreContUrg,ciudadContUrg,idPaisContUrg,emailContUrg,celularContUrg,telefonoContUrg,contacto14dias,sintomas,hospital14dias,animalesVivos14Dias")] FormularioPrincipal formularioPrincipal)
        {
            if (ModelState.IsValid)
            {
                db.Entry(formularioPrincipal).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(formularioPrincipal);
        }

        // GET: FormularioPrincipal/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FormularioPrincipal formularioPrincipal = db.FormularioPrincipal.Find(id);
            if (formularioPrincipal == null)
            {
                return HttpNotFound();
            }
            return View(formularioPrincipal);
        }

        // POST: FormularioPrincipal/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            FormularioPrincipal formularioPrincipal = db.FormularioPrincipal.Find(id);
            db.FormularioPrincipal.Remove(formularioPrincipal);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
