﻿using Sanit.Clases;
using SanitEntity;
using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Configuration;
using System.Web.Mvc;

namespace Sanit.Controllers
{
    public class HomeController : Controller
    {
        private FormSanitEntities db = new FormSanitEntities();
        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.idPaisPermanente = new SelectList(db.Paises.ToList(), "idPais", "Pais");
            ViewBag.idPaisTemp = new SelectList(db.Paises.ToList(), "idPais", "Pais");
            ViewBag.idPaisContUrg = new SelectList(db.Paises.ToList(), "idPais", "Pais");
            ViewBag.idPaisTransitado = new SelectList(db.Paises.ToList(), "idPais", "Pais");
            return View();
        }
        [HttpPost]
        public JsonResult Index(FormularioPrincipal formulario, Compañeros_Familia[] familia, Compañeros_NoFamilia[] grupo, PaisesTransitados[] paisesTransitados, Sintomaticos[] sintomaticos)
        {
            try
            {
                formulario.codigoEdicion = GenerarAleatorio();
                formulario.creado = DateTime.Now;
                db.FormularioPrincipal.Add(formulario);
                db.SaveChanges();

                GuardarDetalles(false, formulario, familia, grupo, paisesTransitados, sintomaticos);

                var mensaje = "<p>Gracias por completar el formulario!<br/>" +
                                "Su codigo de edicion es: <b>" + formulario.codigoEdicion + "</b><br/>" +
                                "O puede acceder a modificar sus datos en el siguiente enlace:<br/>" +
                                String.Format("<a href='https://mdn.dinac.gov.py:2025/Home/EditarPasajero/{0}'>Formulario Sanitario de Ingreso al Paraguay</a><br/><br/></p>", formulario.codigoEdicion);

                var correo = Comun.EnviarCorreo(formulario.correoElectronico, mensaje);


                if (correo == false)
                {
                    return Json(new { result = 1, codigo = formulario.codigoEdicion });
                } //result 1, problema con el correo
                else
                {
                    return Json(new { result = 0, codigo = formulario.codigoEdicion });
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Error inesperado, contacte con el administrador: " + ex.Message);
                return null;
            }
        }
        [HttpGet]
        public JsonResult getPasajero(string codigo)
        {
            FormularioPrincipal Pasajero = db.FormularioPrincipal.Where(o => o.codigoEdicion == codigo).FirstOrDefault();
            var horaBloqueo = Convert.ToUInt32(WebConfigurationManager.AppSettings["horaBloqueo"]);
            var fechaActual = DateTime.Now;

            if (Pasajero != null)
            {
                var fechaLlegada = Pasajero.fechaLlegada;
                var horasRestantes = (fechaLlegada - fechaActual).Value.TotalHours;

                if (horasRestantes < horaBloqueo)
                {
                    var result = "1"; //Edicion Bloqueada
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var result = "0";
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
            }
            else if (Pasajero == null)
            {
                var result = "2"; //El formulario no existe
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var result = "3";//Otro error
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult EditarDatosPasajero(string codigo)
        {
            //Datos de Pasajero
            FormularioPrincipal Pasajero;
            Pasajero = db.FormularioPrincipal.Where(o => o.codigoEdicion == codigo).FirstOrDefault();

            if (Pasajero == null)
            {
                var result = 1;
                return Json(result);
            }
            if (Pasajero != null)
            {
                var id = Pasajero.idpasajero;
                ViewBag.fechaLlegada = Pasajero.fechaLlegada.Value.ToString("yyyy-MM-dd");
                ViewBag.idPaisPermanente = new SelectList(db.Paises.ToList(), "idPais", "Pais", Pasajero.idPaisPermanente);
                ViewBag.idPaisTemp = new SelectList(db.Paises.ToList(), "idPais", "Pais", Pasajero.idPaisTemp);
                ViewBag.idPaisContUrg = new SelectList(db.Paises.ToList(), "idPais", "Pais", Pasajero.idPaisContUrg);
                ViewBag.idPaisTransitado = new SelectList(db.Paises.ToList(), "idPais", "Pais");
                //if (Pasajero.contacto == "S") { ViewBag.contactocovidpositivo.checked; }
                //Datos de Familia
                var familia = db.Compañeros_Familia.Where(o => o.idPasajero == id).ToList();
                ViewBag.familia = familia;
                //Datos de Grupo
                var grupo = db.Compañeros_NoFamilia.Where(o => o.idPasajero == id).ToList();
                ViewBag.nombreGrupo = grupo.Count() == 0 ? "" : grupo.FirstOrDefault().nombre;
                ViewBag.grupo = grupo;
                //Datos de sintomaticos
                var sintomaticos = db.Sintomaticos.Where(o => o.idPasajero == id).ToList();
                ViewBag.sintomaticos = sintomaticos;
                //Datos de Paises
                var paises = db.PaisesTransitados.Where(o => o.idPasajero == id).ToList();
                ViewBag.paises = paises;
            }
            return View(Pasajero);
        }
        [HttpPost]
        public JsonResult EditarDatosPasajero(FormularioPrincipal Pasajero, Compañeros_Familia[] familia, Compañeros_NoFamilia[] grupo, PaisesTransitados[] paisesTransitados, Sintomaticos[] sintomaticos)
        {

            Pasajero.editado = DateTime.Now;
            db.Entry(Pasajero).State = EntityState.Modified;
            db.SaveChanges();
            GuardarDetalles(true, Pasajero, familia, grupo, paisesTransitados, sintomaticos);

            return Json(new { result = 0, codigo = Pasajero.codigoEdicion });

        }
        public void GuardarDetalles(bool edicion, FormularioPrincipal formulario, Compañeros_Familia[] familia, Compañeros_NoFamilia[] grupo, PaisesTransitados[] paisesTransitados, Sintomaticos[] sintomaticos)
        {
            try
            {
                var nro_item = 0;
                var allRec1 = db.Compañeros_Familia.Where(x => x.idPasajero == formulario.idpasajero); //Familia
                var allRec2 = db.Compañeros_NoFamilia.Where(x => x.idPasajero == formulario.idpasajero); //Grupo
                var allRec3 = db.PaisesTransitados.Where(x => x.idPasajero == formulario.idpasajero);//Paises
                var allRec4 = db.Sintomaticos.Where(x => x.idPasajero == formulario.idpasajero);//Sintomaticos

                if (edicion == true)
                {// si es edicion elimino los datos antiguos, sino agrego
                    if (familia != null)
                    {
                        db.Compañeros_Familia.RemoveRange(allRec1);
                    }
                    if (grupo != null)
                    {
                        db.Compañeros_NoFamilia.RemoveRange(allRec2);
                    }
                    if (paisesTransitados != null)
                    {
                        db.PaisesTransitados.RemoveRange(allRec3);
                    }
                    if (sintomaticos != null)
                    {
                        db.Sintomaticos.RemoveRange(allRec4);
                    }
                    db.SaveChanges();
                }

                if (familia != null)
                {
                    foreach (var f in familia)
                    {
                        nro_item = nro_item + 1;
                        var family = new Compañeros_Familia();
                        family.idPasajero = formulario.idpasajero;
                        family.nro_item = nro_item + 1;
                        family.apellido = f.apellido;
                        family.nombre = f.nombre;
                        family.nroAsiento = f.nroAsiento;
                        family.edad = f.edad;
                        db.Compañeros_Familia.Add(family);
                        db.SaveChanges();
                    }
                    nro_item = 0;
                }
                if (grupo != null)
                {
                    foreach (var g in grupo)
                    {
                        nro_item = nro_item + 1;
                        var compas = new Compañeros_NoFamilia();
                        compas.idPasajero = formulario.idpasajero;
                        compas.nro_item = nro_item + 1;
                        compas.grupo = g.grupo;
                        compas.nombre = g.nombre;
                        compas.apellido = g.apellido;
                        db.Compañeros_NoFamilia.Add(compas);
                        db.SaveChanges();
                    }
                    nro_item = 0;
                }
                if (paisesTransitados != null)
                {
                    foreach (var p in paisesTransitados)
                    {
                        nro_item = nro_item + 1;
                        var pais = new PaisesTransitados();
                        pais.idPasajero = formulario.idpasajero;
                        pais.nro_item = nro_item;
                        pais.idPais = p.idPais;
                        db.PaisesTransitados.Add(pais);
                        db.SaveChanges();
                    }
                    nro_item = 0;
                }
                if (sintomaticos != null)
                {
                    foreach (var s in sintomaticos)
                    {
                        nro_item = nro_item + 1;
                        var sint = new Sintomaticos();
                        sint.idPasajero = formulario.idpasajero;
                        sint.nro_item = nro_item + 1;
                        sint.nombre = s.nombre;
                        sint.apellido = s.apellido;
                        sint.sintomas = s.sintomas;
                        db.Sintomaticos.Add(sint);
                        db.SaveChanges();
                    }
                    nro_item = 0;
                }

                //Carga Reporte
                Imprimir(formulario);
            }
            catch (Exception ex)
            {
                throw new Exception();
            }
        }

        public ActionResult Impresion(string codigo)
        {
            ViewBag.codigo = codigo;
            return View();
        }
        public void Imprimir(FormularioPrincipal formulario)
        {
            {
                try
                {
                    Sanit.Reportes.FormDataSet dts = new Reportes.FormDataSet();
                    //Datatable Formulario Principal
                    var dtCab = new Sanit.Reportes.FormDataSet.FormularioPrincipalDataTable();
                    var rowCab = dtCab.NewFormularioPrincipalRow();
                    //Datatable Familia
                    var dtFamilia = new Sanit.Reportes.FormDataSet.FamiliaDataTable();
                    var rowFamilia = dtFamilia.NewFamiliaRow();
                    //Datatable Grupo
                    var dtGrupo = new Sanit.Reportes.FormDataSet.NoFamiliaDataTable();
                    var rowGrupo = dtGrupo.NewNoFamiliaRow();
                    //Datatable Sintomaticos
                    var dtSintomaticos = new Sanit.Reportes.FormDataSet.SintomaticosDataTable();
                    var rowSint = dtSintomaticos.NewSintomaticosRow();

                    #region Formulario
                    var paisesTransitados = db.PaisesTransitados.Where(x => x.idPasajero == formulario.idpasajero).ToList();
                    //Carga de Cabecera
                    rowCab = dtCab.NewFormularioPrincipalRow();
                    rowCab.lineaAerea = formulario.lineaAerea;
                    rowCab.numeroVuelo = formulario.numeroVuelo;
                    rowCab.numeroAsiento = formulario.numeroAsiento;
                    rowCab.fechaLlegada = formulario.fechaLlegada.ToString();
                    rowCab.nroDocumento = formulario.nroDocumento;
                    rowCab.apellido = formulario.apellido;
                    rowCab.nombre = formulario.nombre;
                    rowCab.sexo = formulario.sexo;
                    rowCab.celular = formulario.celular;
                    rowCab.telefonoTrabajo = formulario.telefonoTrabajo;
                    rowCab.telefonoDomicilio = formulario.telefonoDomicilio;
                    rowCab.correoElectronico = formulario.correoElectronico;
                    rowCab.direccionPermanente = formulario.direccionPermanente;
                    rowCab.numeroCasaPermanente = formulario.numeroCasaPermanente;
                    rowCab.ciudadPermanente = formulario.ciudadPermanente;
                    rowCab.depEstProvPermanente = formulario.depEstProvPermanente;
                    var paisP = db.Paises.Where(o => o.idPais == (int)formulario.idPaisPermanente).FirstOrDefault();
                    rowCab.paisPermanente = paisP.Pais;
                    rowCab.codigoPostalPermanente = formulario.codigoPostalPermanente;
                    rowCab.nombreHotelTemp = formulario.nombreHotelTemp;
                    rowCab.direccionHotelTemp = formulario.direccionHotelTemp;
                    rowCab.numPiso = formulario.numPiso;
                    rowCab.ciudadTemp = formulario.ciudadTemp;
                    rowCab.depEstProvPermanente = formulario.depEstProvPermanente;
                    if (formulario.idPaisTemp != null)
                    {
                        var paisT = db.Paises.Where(o => o.idPais == (int)formulario.idPaisTemp).FirstOrDefault();
                        rowCab.paisTemp = paisT.Pais;
                    }
                    else
                    {
                        rowCab.paisTemp = "";
                    }
                    rowCab.codigoPostalTemp = formulario.codigoPostalTemp;
                    rowCab.apellidoContUrg = formulario.apellidoContUrg;
                    rowCab.nombreContUrg = formulario.nombreContUrg;
                    rowCab.ciudadContUrg = formulario.ciudadContUrg;
                    var paisCU = db.Paises.Where(o => o.idPais == (int)formulario.idPaisContUrg).FirstOrDefault();
                    rowCab.paisContUrg = paisCU.Pais;
                    rowCab.correoContUrg = formulario.emailContUrg;
                    rowCab.celularContUrg = formulario.celularContUrg;
                    rowCab.telContUrg = formulario.telefonoContUrg;
                    rowCab.sintomas = formulario.sintomas;
                    rowCab.contacto14dias = formulario.contacto;
                    rowCab.hospital14dias = formulario.hospital;
                    rowCab.animalesVivos14Dias = formulario.mercado;
                    rowCab.motivo = formulario.motivo;
                    var paisesT = "";
                    if (paisesTransitados != null)
                    {
                        foreach (var item in paisesTransitados)
                        {
                            if (paisesT != "") { paisesT = paisesT + " - "; }
                            var pais = db.Paises.Where(x => x.idPais == item.idPais).FirstOrDefault().Pais;
                            paisesT = paisesT + pais;
                        }
                    }
                    rowCab.paises = paisesT;
                    rowCab.creado = formulario.creado.ToString();
                    rowCab.editado = formulario.editado.ToString();
                    rowCab.codigoEdicion = formulario.codigoEdicion;
                    dtCab.Rows.Add(rowCab);

                    #endregion
                    #region Familia
                    //Carga de Familia

                    var familia = db.Compañeros_Familia.Where(x => x.idPasajero == formulario.idpasajero).ToList();
                    if (familia != null)
                    {
                        foreach (var item in familia)
                        {
                            rowFamilia = dtFamilia.NewFamiliaRow();
                            rowFamilia.nombre = item.nombre;
                            rowFamilia.apellido = item.apellido;
                            rowFamilia.nroAsiento = item.nroAsiento;
                            rowFamilia.edad = (item.edad.HasValue ? item.edad : 0).ToString();
                            dtFamilia.Rows.Add(rowFamilia);
                        }
                        dts.Tables["Familia"].Merge(dtFamilia);
                    }
                    #endregion
                    #region Grupo
                    //Carga de Grupo
                    var grupo = db.Compañeros_NoFamilia.Where(x => x.idPasajero == formulario.idpasajero).ToList();
                    if (grupo != null)
                    {
                        foreach (var item in grupo)
                        {
                            rowGrupo = dtGrupo.NewNoFamiliaRow();
                            rowGrupo.grupo = item.grupo;
                            rowGrupo.nombre = item.nombre;
                            rowGrupo.apellido = item.apellido;
                            dtGrupo.Rows.Add(rowGrupo);
                        }
                        dts.Tables["NoFamilia"].Merge(dtGrupo);
                    }
                    #endregion
                    #region Sintomaticos
                    //Carga de Sintomaticos
                    var sintomaticos = db.Sintomaticos.Where(x => x.idPasajero == formulario.idpasajero).ToList();
                    if (sintomaticos != null)
                    {
                        foreach (var item in sintomaticos)
                        {
                            rowSint = dtSintomaticos.NewSintomaticosRow();
                            rowSint.nombre = item.nombre;
                            rowSint.apellido = item.apellido;
                            rowSint.sintomas = item.sintomas;
                            dtSintomaticos.Rows.Add(rowSint);
                        }
                        dts.Tables["Sintomaticos"].Merge(dtSintomaticos);
                    }
                    #endregion

                    //Carga de los datatables en el dataset
                    dts.Tables["FormularioPrincipal"].Merge(dtCab);

                    Sanit.Reportes.Comun.ReportesParametros Pmt = new Reportes.Comun.ReportesParametros();
                    Pmt.ReportPath = Server.MapPath("~/Reportes/Formulario.rpt");
                    Pmt.ReportSource = (object)dts;
                    Pmt.NombreArchivo = "Formulario";
                    Reportes.Comun.ReportParameters.DatosReporte = Pmt;
                }
                catch (Exception e)
                {
                    throw new Exception();
                }
            }
        }
        public string GenerarAleatorio()
        {
            string codigo = string.Empty;
            string[] letras = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z",
                                "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
            Random EleccionAleatoria = new Random();

            int numeroAleatorio = 0;

            for (int i = 0; i < 3; i++)
            {
                int LetraAleatoria = EleccionAleatoria.Next(0, letras.Length);
                numeroAleatorio = EleccionAleatoria.Next(0, 9);
                //Cargo la variable con letra y numero
                codigo += letras[LetraAleatoria];
                codigo += numeroAleatorio.ToString();
            }

            // Validar que no exista el codigo para otro pasajero

            var existe = db.FormularioPrincipal.ToList().Exists(e => e.codigoEdicion == codigo);

            if (existe == true)
            {
                codigo = GenerarAleatorio();
            }

            return codigo;

        }

        public ActionResult PruebaCorreo()
        {
            return View();
        }
        [HttpPost]
        public ActionResult PruebaCorreo(string correo)
        {
            var aleatorio = "x0t0u0";
            var mensaje = "<p>Gracias por completar el formulario!<br/>" +
                                "Su codigo de edicion es: <b>" + aleatorio + "</b><br/>" +
                                "O puede acceder a modificar sus datos en el siguiente enlace:<br/>" +
                                String.Format("<a href='https://mdn.dinac.gov.py:2025/Home/EditarPasajero/{0}'>Formulario Sanitario de Ingreso al Paraguay</a><br/><br/></p>", aleatorio);
            if (correo == "") { correo = "marinainsaurralde11@gmail.com"; }
            Comun.EnviarCorreo(correo, mensaje);

            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}