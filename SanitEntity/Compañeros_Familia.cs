//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SanitEntity
{
    using System;
    using System.Collections.Generic;
    
    public partial class Compañeros_Familia
    {
        public int idCompFamilia { get; set; }
        public int idPasajero { get; set; }
        public Nullable<int> nro_item { get; set; }
        public string apellido { get; set; }
        public string nombre { get; set; }
        public string nroAsiento { get; set; }
        public Nullable<int> edad { get; set; }
    
        public virtual FormularioPrincipal FormularioPrincipal { get; set; }
    }
}
